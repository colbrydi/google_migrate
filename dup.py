import json
from collections import defaultdict

# Load the JSON data
with open('drive_files.json', 'r') as f:
    files = json.load(f)

# Dictionary to store filenames and their paths
file_dict = defaultdict(list)

# Populate the dictionary with filenames and paths
def add_files(file_list):
    for file in file_list:
        if 'MimeType' in file and file['MimeType'] == 'application/vnd.google-apps.folder':
            # It's a folder, recursively add files
            add_files(file['Contents'])
        else:
            filename = file['Path'].split('/')[-1]  # Get the filename
            file_dict[filename].append((file['Path'], file['ID']))
# Call the function with the top-level files
add_files(files)

# Find and print duplicate filenames
duplicates = {filename: paths for filename, paths in file_dict.items() if len(paths) > 1}

if duplicates:
    print("Duplicate filenames found:")
    for filename, paths in duplicates.items():
        duppath = dict()
        for path in paths:
            if path[0] in duppath:
                duppath[path[0]].append(path[1]) 
            else:
                duppath[path[0]] = [ path[1] ] 
        for path in duppath:
            if len(duppath[path]) > 1:
                extlen = len(path.split('.')[-1])+1
                for id in duppath[path]:
                    print(f'rclone backend copyid Google: {id} Google:"/{path[:-extlen]}_{id[-5:]}{path[-extlen:]}"')
                    print(f'rclone delete Google:"/{path}"')
                print(f"### {duppath[path]} - {path}")
else:
    print("No duplicate filenames found.")

