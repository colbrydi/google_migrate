# Google Migration

I want to put an entire backup of all of my google files on my hpcc homespace so that I can start deleteing files and not worrying about loosing things.  There are two ways to transfer files files easily from Google to the HPCC:

1. **_Rclone_**: Command line tool that is installed on the HPCC.
2. **_Globus Online_**: Web based tool.

I tried both and immediatly suffered from a problem where google will store two files in the same directory with the same name.  This is fine in google which uses a file ID to differenciate the files but almost any other file system this is not allowed.  

To fix this problem I needed to know how bad the problem was so I used the rclone command to identify problems.  This is a three step process. First, set up rclone using the "rclone config" command.  I used almost all of the defaults and it seemed to work.  Here is a link to the instrucitons I used:

- <https://docs.icer.msu.edu/Rclone_-_rsync_for_cloud_storage> 
- <https://rclone.org/drive/>
 
Second, download all of the files, their paths and their IDs from google using rclone:

```
srouce mk_drive_files.sh
```

Finnally, check the filenames/paths for duplicates and print them out with their unique IDs.  For this setp I used the following python code modified from suggestions made by ChatGPT:

```
python dup.py
```

This program will output the command I think I need to make a unique name for eacy copy of a duplicate file and save them in the same directory. Then delete the common name. Unfortunatly this only works for files for which I have permissions to delete so there is a problem if I am trying to copy a shared drive.  My next step is to figure out how to handel shared drives. I probably shouldn't be deleteing files form them.  Sometimes I can't even make new files.  
